phoneStoreControllers.directive('navBar', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'NavBar/Views/navBar.html'
    };
});