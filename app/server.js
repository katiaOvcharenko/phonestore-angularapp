var express = require('express'),
    _ = require("underscore"),
    app = express(),
    bodyParser = require('body-parser');


app.use('/', express.static(__dirname));

app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function (request, response) {
    response.sendFile('app/index.html', { root: __dirname });
});

app.listen(3000);
console.log("\nServer start on 127.0.0.1:3000\n");