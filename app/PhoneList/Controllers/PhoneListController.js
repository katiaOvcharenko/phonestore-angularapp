phoneStoreControllers.controller('PhoneListCtrl', ['$scope', '$http', '$rootScope', 'cart',
    function ($scope, $http, $rootScope, cart) {

        $http.get('phones/phones.json').success(function (data) {
            $scope.phones = data;
            $scope.orderProp = '';
        });

        console.log($scope.cartContent);

        $scope.addPhoneToCart = function () {
            console.log('from list: ' + cart);
            cart.addPhone(this.phone);
        }

    }]);