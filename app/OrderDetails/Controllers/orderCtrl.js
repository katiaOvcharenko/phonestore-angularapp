phoneStoreControllers.controller('orderCtrl', ['$scope',
    function ($scope) {

        $scope.confirmOrder = function (order) {
            $scope.message = {
                name: order.name,
                email: order.email,
                phone: order.phone
            }
        };
    }
]);