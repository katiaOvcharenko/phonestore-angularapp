'use strict';

var phoneStore = angular.module('phoneStore', [
    'ui.router',
    'phoneStoreControllers',
    'phoneStoreFilters',
    'cart'
])

    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/phones');

        $stateProvider
            .state('home', {
                url: '/phones',
                templateUrl: 'PhoneList/Views/PhoneList.html',
                controller: 'PhoneListCtrl'
            })
            .state ('success', {
                url: "/phones/success",
                templateUrl: 'OrderDetails/Views/orderSuccess.html',
                controller: 'orderCtrl'
            })
            .state('order', {
                url: '/phones/orderDetails',
                templateUrl: 'OrderDetails/Views/orderDetails.html',
                controller: 'orderCtrl'
            })
            .state('cart', {
                url: '/phones/cartContent',
                templateUrl: 'CartContent/Views/cartContent.html',
                controller: 'cartSummaryController'
            })
            .state('about', {
                url: '/phones/about',
                templateUrl: 'NavBar/Views/about.html',
                controller: 'NavBarCtrl'
            }).
            state('contactUs', {
                url: '/phones/contactUs',
                templateUrl: 'NavBar/Views/contactUs.html',
                controller: 'NavBarCtrl'
            }).
            state('phone', {
                url: "/phones/:phoneId",
                templateUrl: 'PhoneDetail/Views/PhoneDetail.html',
                controller: 'PhoneDetailCtrl'
            })
    }]);

var phoneStoreControllers = angular.module('phoneStoreControllers', []);

//var myApp = angular.module('myApp', [
//    'ngRoute',
//    'myAppControllers',
//    'myAppFilters'
//])
//    .config(['$routeProvider', function($routeProvider) {
//    $routeProvider.
//        when('/phones/orderDetails', {
//            templateUrl: 'Views/orderDetails.html',
//            controller: 'orderCtrl'
//        }).
//        when('/phones/cartContent', {
//            templateUrl: "Views/cartContent.html",
//            controller: "CartCtrl"
//        }).
//        when('/phones/about', {
//            templateUrl: 'Views/about.html',
//            controller: 'NavBarCtrl'
//        }).
//        when('/phones/contactUs', {
//            templateUrl: 'Views/contactUs.html',
//            controller: 'NavBarCtrl'
//        }).
//        when('/phones', {
//            templateUrl: 'Views/PhoneList.html',
//            controller: 'PhoneListCtrl'
//        }).
//        when('/phones/:phoneId', {
//            templateUrl: 'Views/PhoneDetail.html',
//            controller: 'PhoneDetailCtrl'
//        }).
//        otherwise({
//            redirectTo: '/phones'
//        });
//}]);
//
