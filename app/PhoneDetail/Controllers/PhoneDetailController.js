phoneStoreControllers.controller('PhoneDetailCtrl', ['$scope', '$stateParams', '$http', 'cart',
    function ($scope, $stateParams, $http, cart) {

        $http.get('phones/' + $stateParams.phoneId + '.json').success(function (data) {
            $scope.phone = data;
            $scope.mainImageUrl = data.images[0];
        });

        $scope.addPhoneToCart = function () {
            console.log('from details: ' + cart);
            cart.addPhone($scope.phone);
        };

        $scope.setImage = function(imageUrl) {
            console.log(imageUrl);
            $scope.mainImageUrl = imageUrl;
            console.log(imageUrl);
        };

        $scope.addReview = function (reviewDetails) {
            $scope.message = {
                name: reviewDetails.name,
                email: reviewDetails.email,
                comment: reviewDetails.comment
            }
        };
    }]);



