phoneStoreControllers.controller("cartSummaryController", function($scope, cart) {
$scope.cartContent = cart.getPhones();
$scope.total = function () {
    var total = 0;
    for (var i = 0; i < $scope.cartContent.length; i++) {
        total += ($scope.cartContent[i].price * $scope.cartContent[i].count);
    }
    return total;
};

$scope.remove = function (id) {
    cart.removePhone(id);
}
});

angular.module("cart", [])
    .factory("cart", function () {
        var cartContent = [];

        return {
            addPhone: function (id, name, price) {
                var addedTo = false;
                for (var i = 0; i < cartContent.length; i++) {
                    if (cartContent[i].id == id) {
                        cartContent[i].count++;
                        addedTo = true;
                        break;
                    }
                }
                if (!addedTo) {
                    cartContent.push({
                        count: 1, id: id, price: price, name: name
                    });
                }
            },
            removePhone: function (id) {
                for (var i = 0; i < cartContent.length; i++) {
                    if (cartContent[i].id == id) {
                        cartContent.splice(i, 1);
                        break;
                    }
                }
            },
            getPhones: function () {
                return cartContent;
            }
        }
    })

    .directive("cartSummary", function (cart) {
        return {
            restrict: "E",
            templateUrl: "CartContent/Views/cartSummary.html",
            controller: function ($scope) {
                var cartContent = cart.getPhones();
                $scope.total = function () {
                    var total = 0;
                    for (var i = 0; i < cartContent.length; i++) {
                        total += (cartContent[i].id.price * cartContent[i].count);
                    }
                    return total;
                };
                $scope.itemCount = function () {
                    var total = 0;
                    for (var i = 0; i < cartContent.length; i++) {
                        total += cartContent[i].count;
                    }
                    return total;
                }
            }
        };
    });

//.directive("cartSummary", function (cart) {
//    return {
//        restrict: "E",
//        templateUrl: "CartContent/Views/cartSummary.html",
//        controller: function ($scope) {
//            var cartContent = cart.getPhones();
//            $scope.total = function () {
//                var total = 0;
//                for (var i = 0; i < cartContent.length; i++) {
//                    total += (cartContent[i].id.price * cartContent[i].count);
//                }
//                return total;
//            };
//            $scope.itemCount = function () {
//                var total = 0;
//                for (var i = 0; i < cartContent.length; i++) {
//                    total += cartContent[i].count;
//                }
//                return total;
//            }
//        }
//    };
//});